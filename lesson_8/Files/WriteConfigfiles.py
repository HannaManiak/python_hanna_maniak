import json, random
main_keys = ['web_server', 'http_server', 'nginx']
# keys = {'url':'a', 'ipv4':'d', 'name':'v', 'root_user':'s', 'root_password':'r'}
#keys = ['url', 'ipv4', 'name', 'root_user','root_password']


def simple():
    main_keys = ['web_server', 'http_server', 'nginx']
    for i in main_keys:
        with open('{}{}'.format(i, '.json'), 'w') as f:
            my_dict = {}
            my_dict['url'] = '{}{}'.format(i, random.randrange(1, 100))
            my_dict['ipv4'] = '{}'.format(random.randrange(1, 100))
            my_dict['name'] = i
            my_dict['root_user'] = 'root'
            my_dict['root_password'] = random.randrange(1, 10000)
            my_object = json.dumps(my_dict)
            f.write(my_object)
