# -*- coding: utf-8 -*-
class Test:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)# добавить для селф любое количество именнованных ключ - значение

my_class = Test(name = 'Anya', surname = 'Maniak')
print(my_class.name)

class Test1:
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k,v)

my_class1 = Test1(age = 22, city = 'Novopolotsk')
print(my_class1.age)

class Test1:
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k,v)
class Store:
    store = []
    @classmethod
    def add(cls):
        cls.store.append(1)

a = Store()
a.add()
print(a.store)
