import json
class Dict_save:
    my_dict = {}

    def add_key_value(self, key, value):
        if isinstance(key, str) and isinstance(value, (list, tuple, set, dict, int, str)):
            if not key in Dict_save.my_dict:
                Dict_save.my_dict[key] = value
            else:
                print('This key is already exist')
        else:
            print('Wrong key or value')

    def update_key_value(self, key, value):
        if isinstance(key, str) and isinstance(value, (list, tuple, set, dict, int, str)):
            if key in Dict_save.my_dict:
                Dict_save.my_dict[key] = value
            else:
                print('This key is not exist')
        else:
            print('Wrong key or value')


    def remove_key(self, key):
        if Dict_save.my_dict:
            if key in Dict_save.my_dict:
                Dict_save.my_dict.pop(key)
            else:
                print ('No key')
        else:
            print ('No data')



    def write_keys(self, path):
        with open(path, 'a') as f:
            json_object = json.dumps(Dict_save.my_dict)
            f.write(json_object)





a = Dict_save()
a.add_key_value('Hanna', 'Maniak')
print(a.my_dict)
a.add_key_value('Hanna', 'bad')
a.add_key_value('Hanna1', 'bad')
print(a.my_dict)
a.update_key_value('Hanna', 'WOOOW')
print(a.my_dict)

# a.add_key_value('Petya', 'weather')
# print(a.my_dict)
# a.remove_key('Hanna')
# print(a.my_dict)
# a.write_keys('D:/testik.json')


