import math


# 1 task
def vol(length, width, height):
    return length * width * height
print (vol(2,3,2))


# 2 task
def parameters(length, width):
    return length + width, length * width
print (parameters(2,10))


# task 3
def summa(*args):
    return int(math.fsum(args))
print (summa(2,10))


# task 4
def simple(param, **kwargs):
    if param == 'keys':
        return kwargs.keys()
    elif param == 'values':
        return kwargs.values()
    else:
        return "Param is not exist"
print (simple(param='values', a='good', b='morning'))


# task 5
def f1(x):
    def f2(y):
        return x + y
    return f2
f2 = f1(100)
print (f2(200))


# task 6
def simple(*args, **kwargs):
    key_list = kwargs.keys()
    for key in key_list:
        if hasattr(kwargs[key], '__call__'):
            kwargs[key](*args)


def printer(*args):
    print('Length of arguments is ' + str(len(args)))


def summa(*args):
    print('Sum of arguments is ' + str(int(math.fsum(args))))
simple(1, 2, 2, 3, funk1=printer, funk2=summa)




