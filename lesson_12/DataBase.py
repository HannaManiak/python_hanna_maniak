import json, os

class DataBase:
    my_db = []

class EditDataBase(DataBase):

    @classmethod
    def all(cls):
        filepath = 'test.json'
        if os.path.exists(filepath):
            if os.path.exists('test.json'):
                with open(filepath, 'r') as f:
                    my_list = json.load(f)
                    for i in my_list:
                        for item in i:
                            DataBase.my_db.append(item)
            return DataBase.my_db
        else:
            print (DataBase.my_db)

    @classmethod
    def dump(cls, filepath):
        if os.path.exists('test.json'):
            with open(filepath, 'r') as f:
                my_list = json.load(f)
                my_list.append(DataBase.my_db)
            with open(filepath, 'w') as f:
                write_object = json.dumps(my_list)
                f.write(write_object)
        else:
            with open(filepath, 'w') as f:
                write_object = json.dumps(DataBase.my_db)
                f.write(write_object)

    @classmethod
    def load(cls):
        filepath = 'test.json'
        if os.path.exists(filepath):
            with open(filepath, 'r') as f:
                my_list = json.load(f)
                return my_list
        else:
            print ('No data in file')

    @classmethod
    def insert(cls, **kwargs):
        if len(DataBase.my_db)>=20:
            filepath = 'test.json'
            if os.path.exists(filepath):
                with open(filepath, 'r') as f:
                    my_list = json.load(f)
                    my_list.append(DataBase.my_db)
                with open(filepath, 'w') as f:
                    write_object = json.dumps(my_list)
                    f.write(write_object)
            else:
                with open(filepath, 'w') as j:
                    write_object = json.dumps([DataBase.my_db])
                    j.write(write_object)
            DataBase.my_db = []
        DataBase.my_db.append(kwargs)

    @classmethod
    def select(cls, **kwargs):
        select_list = DataBase.my_db[:]  # make a copy of KeyValueStorage.my_list (function copy() didn't work)
        for one_dict in DataBase.my_db:
            dict_items = one_dict.items()
            for item in kwargs.items():
                if item not in dict_items:
                    select_list.remove(one_dict)
                    break
            else:
                continue
        print ('Select objects are {}'.format(select_list))

    @classmethod
    def delete(cls, **kwargs):
        select_list = DataBase.my_db[:]  # make a copy of KeyValueStorage.my_list (function copy() didn't work)
        for one_dict in DataBase.my_db:
            dict_items = one_dict.items()
            for item in kwargs.items():
                if item not in dict_items:
                    select_list.remove(one_dict)
                    break
            else:
                continue
        for i in select_list:
            DataBase.my_db.remove(i)

    @classmethod
    def update_list(cls, set, where):
        if isinstance(set, dict) and isinstance(where, dict):
            select_list = DataBase.my_db[:]  # make a copy of KeyValueStorage.my_list (function copy() didn't work)
            for one_dict in DataBase.my_db:
                dict_items = one_dict.items()
                for item in where.items():
                    if item not in dict_items:
                        select_list.remove(one_dict)
                        break
                else:
                    continue
            for i in select_list:
                i.update(set)
        else:
            print ('Wrong types of parameters')





