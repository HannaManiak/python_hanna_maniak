# coding: utf8
import json, os
# Task 1
def f2(x):
    return x

def f1(f2, **kwargs):
    print (f2, kwargs)

# Task 2
class Test:
    def __init__(self, name, surname, *args, **kwargs):
        self.name = name
        self.surname = surname
        self.numbs = args
        self.dictionary = kwargs

# Task 3
    def get_full_name(self):
        print ('{} {}'.format(self.name, self.surname))

    def set_age(self, age):
        self.age = age

# Task 4
    @staticmethod
    def printer(**kwargs):
        print(kwargs)

    @staticmethod
    def nothing():
        pass

# Task 5
# При создании метода класса обязательным аргументом является cls - сам класс,
# а при создании статического метода в качетсве параметра не нужен self - экземпляр класса. Можно вызывать метод через класс и через экземпляр класса.

# Task 6
# with open('test.json', 'w') as j:
#     json_object = json.dumps({"my_car": "bmw", "my_company": "MTZ"})
#     j.write(json_object)
try:
    with open('test.json', 'r') as j:
        my_dict = json.load(j)
        my_dict.update({"my_home": 44, "my_company": "MTZ"})
    with open ('test.json', 'w') as j:
        json_object = json.dumps(my_dict)
        j.write(json_object)
except:
    print('noooo')

# Task 7
# Конкатенация строк - это соединение строк. Реализуется несколькими методами:
'{}, {}'.format('Hello', 'World')
'%s, %s'%('Hello', 'world')

# Task 8
# Конструкция with гарантирует закрытие файла после всех операций, которые написаны внутри конструкции with

# Task 9
# Кортеж занимает в памяти меньше места, лист. Так как кортеж неизменяемая последовательность в отличие от листа. И для листа резервируется еще место.

# Task 10
# Функции преобразования типов преобразуют один тип в другой.
# Яркие примеры: str(23) - преобразует число - в строку,  int(123,4) - десятичное число - в целое,  float(123) - целове число - в число с плавающей точкой.

# Task 11
# class Person - базовый класс, class Woman(Person) -  класс, который наследует все атрибуты и методы класса Person и плюс имеет свои.

# Task 12
# При множественном наследовании класс Woman наследует не один а несколько классов. Например, Woman(Person, Work)

# Task 13
# type(Test1, (Test), 'func' lambda x: x*2)

# Task 14
# Функция map(func, seq). Принимает в качестве аргументов некую функцию и последовательность. И для каждого объекта последовательности применяет функциюю Возвращает итерируемый объект.
map(lambda x: x*2, [1,2,3])

# Task 15
# С помощью оператора in проверяют есть ли что-то в чем-то. Например,
my_dict = {'name': 'Hanna', 'home': 22}
if 'home' in my_dict:
    my_dict['home'] = 23

# Task 16
# это словарь
#print(str.__dict__)

# Task 17 - не знаю
def test_func(x):
    print (x)
def test_func1(**x):
    print (x)
# test_func(3)
# test_func1(x=4)

# Test 18
# функция super().__init__() используется в конструкторе класса (Женщина), который наследует другой класс (Человек),
# чтобы сохранить все атрибуты из конструктора класса Человек и добавить собственные.

# Test 19

with open('test.txt', 'r') as j:
    my_strings = j.readlines()
    temp_list = []
    n=0
    r=1
    for string in my_strings:
        n += 1
        temp_list.append(string)
        if n == 20:
            filename = 'file{}.txt'.format(r)
            with open(filename, 'w') as j:
                sep = ''
                my_string = sep.join(temp_list)
                j.write(str(my_string))
        temp_list = []
        n = 0
        r += 1


# Test 20
# git pull - подтягивает данные из удаленного репозитория в локальный

# Test 21



# Test 22
class Car:
    def __init__(self, kuzov, windows, seats, fuel, wheels = 4):
        self.kuzov = kuzov
        self.windows = windows
        self. seats = seats
        self.fuel = fuel
        self.wheels = wheels

class BMW(Car):
    pass

my_car = BMW('universal', 4, 5, 'gas')
my_car.kuzov










