def get_protokol(my_url):
    return my_url.split('://')[0]

def get_host(my_url):
    a = my_url.split('://')[-1]
    host = a.split('/')[0]
    return host

def get_parameters(my_url): # не успела сделать словарем
    a = my_url.split('?')[-1]
    if '#' in a:
        parameters = a.split('#')[0]
        return parameters
    else:
        return a

def search(my_url):
    if '#' in my_url:
        s = my_url.split('#')[-1]
        return s
    else:
        return None



class URL:
    def __init__(self, my_url):
        self.url = my_url
        self.protokol = get_protokol(my_url)
        self.host = get_host(my_url)
        self.parameters = get_parameters(my_url)
        self.search = search(my_url)





url_patterns = URL('http://olol.ru/index.php?page=shop.product_details&color=black#info')
print(url_patterns.protokol)
print(url_patterns.host)
print(url_patterns.parameters)
