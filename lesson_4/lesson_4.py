# coding: utf8
source_dict = {'first_key': 'first_value', 'second_key': 'second_value'}
# 1 task
print (source_dict['first_key'])
# 2 task
source_dict['first_key'] = [1, 2, 4]
print (source_dict)
# 3 task
# Конструкция dict.fromkeys(['key_val_1', 'key_val_2']) создает словарь из списка с ключами. Значения по умолчанию равны None.
my_dict = dict.fromkeys(['key_val_1', 'key_val_2'])
print (my_dict)
# Можно задать знчение по умолчанию: dict.fromkeys(['key_val_1', 'key_val_2'], value)
my_dict = dict.fromkeys(['key_val_1', 'key_val_2'], 123)
print (my_dict)
# 4 task
source_dict = {'first_key': 'first_value', 'second_key': 'second_value'}
new_list = []
for i in source_dict.items():
    i = (i[1],i[0])
    new_list.append(i)
source_dict = dict(new_list)
print (source_dict)
# 5 task
source_dict = {'first_key': 'first_value', 'second_key': 'second_value'}
print (source_dict.keys())
# 6 task
my_tuple = tuple(source_dict.values())
print (my_tuple)
# 7 task
my_list = [[1, 'qwe'], [518, 'ttr'], [56, 'fty0']]
my_list.sort()
print (my_list)
# 8 task
my_list.sort(key=lambda x:x[1], reverse=True)
print (my_list)
# 9 task
my_list = ['Hanna']
print (my_list)
# 10 task
my_list = list('Hanna')
print (my_list)
# 11 task
my_dict = dict((('ff', 30), ('sd','mm')))
print (my_dict)
# 12 task in text file "lesson_4"
# 13 task in text file "lesson_4"
# 14 task
my_list = [[1, 'qwe'], [518, 'ttr'], [56, 'fty0']]
my_tuple = tuple(my_list)
print (my_tuple)

