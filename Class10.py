import math
class Static:
    @staticmethod
    def square(r):
        return round(math.pi*r**2, 2)
    @staticmethod
    def length(r):
        return round(2*math.pi*r, 2)
print(Static.length(2))
print(Static.square(3))
object = Static()
print(object.square(4))


class MyClass:
    @classmethod
    def get_overview(cls, x,y):
        return x + y


print('Try Class MyClass {}'.format(MyClass.get_overview(2, 3)))


class Counter:
    counter = 0

    def update_counter(self):
        Counter.counter +=1

    def set_counter(self):
        self.counter +=1

c, b = Counter(), Counter()
c.set_counter()
b.set_counter()
print(('Counter in Class Counter is {}'.format(c.counter)))
print('Counter in Class Counter is {}'.format(Counter.counter))

class Likes:
    counter = 0
    def set_like(self):
        Likes.counter +=1

    def set_dislike(self):
        Likes.counter -=1

    def get_count_likes(self):
        return Likes.counter
c = Likes()
c.set_like()
c.set_like()
c.set_dislike()
c.set_dislike()
c.set_dislike()
print('Counter in Class Likes is {}'.format(c.get_count_likes()))


class Likes1:
    counter = 0

    @classmethod
    def set_like(cls):
        Counter.counter += 1

    @classmethod
    def set_dislike(cls):
        Counter.counter -= 1

    @classmethod
    def get_likes(cls):
        Counter.counter
b = Likes1()
b.set_like()
b.set_like()
b.set_like()
b.set_dislike()
print('Counter in Class Likes1 is {}'.format(Counter.counter))

class Property:
    counter = 0
    @property
    def update_counter(self):
        self.counter +=1

a = Property()
a.update_counter
a.update_counter
a.update_counter
a.update_counter
print('Counter in Class Property is {}'.format(a.counter))

class Comments:
    Comm_list = []

    def add_comment(self, User, Comment):
        Comments.Comm_list.append('{}: {}'.format(User, Comment))


    def get_first_comment(self):
        if Comments.Comm_list:
            return Comments.Comm_list[0]
        else:
            return 'No comments'
    def get_last_comment(self):
        if Comments.Comm_list:
            return Comments.Comm_list[-1]
        else:
            return 'No comments'
    def get_all_comments(self):
        if Comments.Comm_list:
            return Comments.Comm_list
        else:
            return 'No comments'
    def write_comments(self, path):
        with open(path, 'a') as f:
            a = str(Comments.Comm_list)
            f.write(a)

l = Comments()
l.add_comment('Hanna', 'Like')
print(l.get_first_comment())
l.add_comment('Hanna', 'Like')
l.add_comment('Hanna', 'DisLike')
print (Comments.Comm_list)
print(l.get_first_comment())
print(l.get_last_comment())
print(l.get_all_comments())
l.write_comments('D:/test.txt')



