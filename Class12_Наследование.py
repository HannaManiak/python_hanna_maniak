# -*- coding: utf-8 -*-
# 1
class Base:
    def foo(self):
        print('Hello from base class')

    @classmethod
    def bar(cls):
        print(cls.__class__)


class First(Base):
    def foo(self):
        print('Hello')

f = First()
f.foo()

#2
class Mydict(dict):
    def get(self, key, default=0):
        return dict.get(self, key, default)

a = Mydict(c=10, f=12)
print (a.get('d'))

#3
class Base:
    def __init__(self):
        self.surname = 'Maniak'
        self.name = 'Hanna'

class First(Base):
    def __init__(self):
        Base.__init__(self)
        self.age = 3

f = First()
print(f.age)

class Base:
    def __init__(self):
        self.surname = 'Maniak'
        self.__name = 'Hanna' # протектор. Можно вызвать только через какой-то метод
    def get(self):
        return self.__name

class First(Base):
    def __init__(self):
        super().__init__()
        self.age = 3

f = First()
print(f.get())

# type базовый класс для всего
# Синглтон - патерн. класс который будет возвращать один и тот же объект
class Singleton:
    instance = None

    def __new__(cls):
        if cls.instance is None:
            cls.instance = super().__init__(cls)
        return cls.instance

a = Singleton()
a1 = Singleton()
print(id(a) == id(a1))
print(a is a1)

# Множественное наследование
class Data:
    data = []
    def __repr__(self):
        return '{} {}'.format(self.__class__.__name__, str(self.data))

class API:
    @classmethod
    def insert(cls, **kwargs):
        cls.data.append(dict(**kwargs))

    @classmethod
    def all(cls):
        return cls.data

class First(Data, API):
    pass

f = First()
f.insert(mama = 'Luda')
print(f.__repr__())
f.insert(mama1 = 'Luda1')
print(f.all())
print(f.__repr__())