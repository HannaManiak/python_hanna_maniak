class Phone:
    def __init__(self, model, seria, color, type='monoblock', screen=5):
        self.model = model
        self.seria = seria
        self.color = color
        self.type = type
        self.size_of_screen = screen

    def get_full_name(self):
        return '{}{} {}'.format(self.model, self.seria, self.color)

    def set_battery(self, battery):
        self.battery = battery

    def set_camera(self, camera):
        self.camera = camera

    def change_color(self, newcolor):
        self.color = newcolor

my_phone = Phone('iphone', 6, 'grey')
print(my_phone.model, my_phone.color, my_phone.type, my_phone.size_of_screen)
print(my_phone.get_full_name())
my_phone.change_color('gold')
print(my_phone.get_full_name())
my_phone.set_battery(1700)
print(my_phone.battery)
my_phone.set_camera(12)
print (my_phone.camera)