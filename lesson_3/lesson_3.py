# coding: utf8
string = "https://test_url.org?repo=1&branch=first&user=test"
# 1 task protokol stands before ://
ind1 = string.find('://')
print (string[:ind1])
# 2 task get parameters stands after ?
ind2 = string.find('?')
print (string[(ind2+1):])
# 3 task
print (string[(ind1+3):(ind2)])
# 4 task
ind4 = string.find('user')
print (string[(ind4+5):] if ind4 > -1 else 'Not exist')
# 5 task
str = 'age:%(age)d, name: %(name)s'%{'age' : 22, 'name' : 'Hanna'}
print (str)
str1 = 'my car is {car} and i live in {city}'.format(car = 'bmw', city = 'London')
print (str1)
# 6 task
sep = ";"
print (sep.join(['hello','world']))
# 7 task
'Today is a wonderful day'
my_string1 = 'Today is %(day)s. It is a wonderful day, when %(name)s goes to the courses of Python'%{'day':'THURSDAY','name':'Hanna'}
my_string2 = "'Good morning', - {p[0]} usually say when they meet each other {p[1]}.".format(p = ('people','every day'))
my_string3 = '{0}{1}{2}'.format(my_string1[0:5], ' finally ', my_string2[42:] )
print(my_string1)
print(my_string2)
print(my_string3)