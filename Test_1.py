# -*- coding: utf-8 -*-
my_str = """Belgian!? "counter" - terror prosecutors are investigating a stabbing in Brussels in which at least two police officers were wounded, officials say.
Federal prosecutor Jennifer Vanderputten told the press:
"We have reason to believe it was a terrorist attack. We can't say more at the moment, because we're gathering information."
The incident took place during a police check in the Schaerbeek area of the Belgian capital. One officer was stabbed in the stomach and one in the neck, Vanderputten said.
Neither victim is said to be in a grave condition.
The assailant is said to have broken the nose of a third police officer, who then shot the suspect in the leg, before arresting him.
Bomb scare
Hours earlier, travellers had been evacuated from the capital's Brussels Nord station over a bomb alert.
The railway station was shut down for an hour while bomb disposal units checked the area."""
first_list = my_str.split(" ")

second_list = []
for item in first_list:
    if item.find("\n")>-1:
        item1, item2 = item.split("\n")[0], item.split("\n")[1]
        second_list.append(item1)
        second_list.append(item2)
    else:
        second_list.append(item)

third_list = []
for item in second_list:
    if item.find('"')==0:
        item = item.split('"')[1]
    if not item.find('"') == 0 and item.find('"') >-1:
        item = item.split('"')[0]
    seps = ['.', ',', ':', ';', '?', '!']
    for sep in seps:
        if item.find(sep) > -1:
            item = item.replace(sep, "")
    third_list.append(item)

for item in third_list:
    if item == '-':
        third_list.remove(item)

my_list = []
for item in third_list:
    item = item.lower()
    my_list.append(item)

my_dict = {}
for item in my_list:
    my_dict[item] = my_list.count(item)
my_dict["all"] = len(my_dict)
print (my_dict)

