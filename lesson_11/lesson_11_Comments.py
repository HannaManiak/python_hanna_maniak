class Comments:
    Comm_list = []

    def add_comment(self, User, Comment):
        Comments.Comm_list.append('{}: {}'.format(User, Comment))

    def get_first_comment(self):
        if Comments.Comm_list:
            return Comments.Comm_list[0]
        else:
            return 'No comments'
    def get_last_comment(self):
        if Comments.Comm_list:
            return Comments.Comm_list[-1]
        else:
            return 'No comments'

    def get_all_comments(self):
        if Comments.Comm_list:
            return Comments.Comm_list
        else:
            return 'No comments'

    def write_comments(self, path):
        with open(path, 'a') as f:
            write_object = str(Comments.Comm_list)
            f.write(write_object)

l = Comments()
print(l.get_first_comment())
l.add_comment('Hanna', 'Like')
print(l.get_first_comment())
l.add_comment('Petya', 'Like')
l.add_comment('Vanya', 'DisLike')
print (Comments.Comm_list)
print(l.get_first_comment())
print(l.get_last_comment())
print(l.get_all_comments())
l.write_comments('D:/test.txt')