class ShortTermStorage:
    my_list = []

    def insert(self, **kwargs):
        ShortTermStorage.my_list.append(kwargs)
        if len(ShortTermStorage.my_list) > 10:
            ShortTermStorage.my_list = []
            ShortTermStorage.my_list.append(kwargs)

    def select(self, **kwargs):
        select_list = ShortTermStorage.my_list[:] # make a copy of ShortTermStorage.my_list (function copy() didn't work)
        for one_dict in ShortTermStorage.my_list:
            dict_items = one_dict.items()
            for item in kwargs.items():
                if item not in dict_items:
                    select_list.remove(one_dict)
                    break
        print ('Select objects are {}'.format(select_list))

    def delete(self, **kwargs):
        select_list = ShortTermStorage.my_list[:]  # make a copy of ShortTermStorage.my_list (function copy() didn't work)
        for one_dict in ShortTermStorage.my_list:
            dict_items = one_dict.items()
            for item in kwargs.items():
                if item not in dict_items:
                    select_list.remove(one_dict)
                    break
        for i in select_list:
            ShortTermStorage.my_list.remove(i)

    def all(self):
        print ('ShortTermStorage.my_list include {}'.format(ShortTermStorage.my_list))



my_class = ShortTermStorage()
my_class.insert(name='Hanna', age=22)
my_class.insert(name='Hanna')
my_class.insert(surname='Maniak')
my_class.insert(key1='Petya', key2='Sasha')
my_class.insert(age=30, work='hard', name='Hanna', surname='Maniak')
my_class.insert(name='Hanna', surname='Maniak')
my_class.all()
my_class.select(name='Hanna', surname='Maniak')
my_class.delete(name='Hanna')
print ('ShortTermStorage.my_list after deleting is {}'.format(ShortTermStorage.my_list))
my_class.all()
