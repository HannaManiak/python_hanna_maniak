# coding: utf8
import json, os

class Dict_save:
    my_dict = {}

    # прочитать все пары ключ:значение (из текущего словаря и из файла)
    def read_all_my_dict(self):
        filepath = 'test.json'
        if os.path.exists(filepath):
            with open('test.json', 'r') as f:
                my_dictionary = json.load(f)
                my_dictionary.update(Dict_save.my_dict)
                Dict_save.my_dict = my_dictionary
                print (Dict_save.my_dict)
        else:
            print (Dict_save.my_dict)

    # добавить ключ (если в текущем словаре больше 2 пар ключ:значение(2 - для наглядности), то записать текущий словарь в файл,
    # обнулить его и добавить ключ уже в пустой словарь)
    def add_key(self, key):
        if len(Dict_save.my_dict) > 2:
            filepath = 'test.json'
            if os.path.exists(filepath):
                with open(filepath, 'r') as f:
                    my_dictionary = json.load(f)
                    my_dictionary.update(Dict_save.my_dict)
                    Dict_save.my_dict = my_dictionary
                    print (Dict_save.my_dict)
            with open(filepath, 'w') as j:
                json_object = json.dumps(Dict_save.my_dict)
                j.write(json_object)
            Dict_save.my_dict = {}
        if isinstance(key, str):
            if key not in Dict_save.my_dict:
                Dict_save.my_dict.update(dict.fromkeys([key]))
            else:
                print('This key already exists')
        else:
            print('Wrong type of key')

    def update_key_value(self, key, value):
        if isinstance(key, str) and isinstance(value, (list, tuple, set, dict, int, str)):
            if key in Dict_save.my_dict:
                Dict_save.my_dict[key] = value
            else:
                print('This key doesnt exist')
        else:
            print('Wrong type of key or value')

    def remove_key(self, key):
        if Dict_save.my_dict:
            if key in Dict_save.my_dict:
                Dict_save.my_dict.pop(key)
            else:
                print ('No key')
        else:
            print ('No data in storage')

    def read_key(self, key):
        if key in Dict_save.my_dict:
            print ('{}: {}'.format(key, Dict_save.my_dict[key]))
        else:
            print ('This key doesnt exist')

    def write_keys(self, path):
        with open(path, 'a') as f:
            json_object = json.dumps(Dict_save.my_dict)
            f.write(json_object)

my_object = Dict_save()
my_object.add_key('name')
my_object.add_key('surname')
my_object.update_key_value('name','hanna')
my_object.update_key_value('surname', 'maniak')
print (Dict_save.my_dict)
my_object.read_all_my_dict()
my_object.update_key_value('mom', 'lovely')
print (Dict_save.my_dict)
my_object.read_all_my_dict()
my_object.add_key("hello")
print (Dict_save.my_dict)
my_object.update_key_value("hello", "world")
my_object.read_all_my_dict()
my_object.add_key("age")
my_object.update_key_value("age", 22)
print (Dict_save.my_dict)
my_object.read_all_my_dict()
